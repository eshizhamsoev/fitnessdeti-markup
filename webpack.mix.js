const mix = require('laravel-mix');
require('laravel-mix-svg-sprite');

mix
  .setPublicPath('dist')
  .js('assets/src/js/app.js', 'js')
  .sass('assets/src/scss/app.scss', 'css')
  .copyDirectory('assets/images', 'dist/images')
  .svgSprite('assets/src/svg', 'sprite.svg');

mix.babelConfig({
  presets: ['@babel/preset-env'],
});
