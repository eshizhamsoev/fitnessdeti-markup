function switchTab(tabs) {
  tabs.querySelectorAll('.js-tab-btn').forEach((e) => {
    e.addEventListener('click', () => {
      const name = e.dataset.tab;
      const reviews = tabs.querySelectorAll(`.js-tab-review[data-tab=${name}]`);
      const reviewsActive = tabs.querySelectorAll('.js-tab-review.active');
      const reviewsAll = tabs.querySelectorAll('.js-tab-review');
      const tabActiveBtn = tabs.querySelector('.js-tab-btn.active');
      if (!e.classList.contains('active')) {
        if (name === 'all') {
          for (const review of reviewsAll) {
            review.classList.add('active');
          }
        } else {
          for (const reviewActive of reviewsActive) {
            reviewActive.classList.remove('active');
          }
          for (const review of reviews) {
            review.classList.add('active');
          }
        }
        tabActiveBtn.classList.remove('active');
        e.classList.add('active');
      }
    });
  });
}

const tabsEl = document.querySelector('.js-find-branch');
if (tabsEl) {
  switchTab(tabsEl);
}

const reviewTabs = document.querySelector('.js-review-tabs');
if (reviewTabs) {
  switchTab(reviewTabs);
}
