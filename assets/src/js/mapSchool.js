import { iconSize, offsetIcon } from './const/map';
import mark from '../images/map-marker.svg';

function initMapSchool() {
  const element = document.querySelector('.js-school-map');

  if (!element) {
    return;
  }

  const { latitude, longitude, name, address } = element.dataset;
  const myMap = new window.ymaps.Map(element, {
    center: [latitude, longitude],
    zoom: 11,
    controls: [],
  });
  const myPlacemark = new window.ymaps.Placemark(
    [latitude, longitude],
    {
      balloonContentHeader: `<div>${name}</div>`,
      balloonContentBody: `<div>${address}</div>`,
    },
    {
      iconLayout: 'default#image',
      iconImageHref: mark,
      iconImageSize: iconSize,
      iconImageOffset: offsetIcon,
    }
  );
  myMap.geoObjects.add(myPlacemark);
}

if (window.ymaps) {
  window.ymaps.ready(initMapSchool);
}
