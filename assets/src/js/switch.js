const switchElement = document.querySelector('.js-switch-slider');
if (switchElement) {
  const radiosInput = document.querySelectorAll('[name="switch"]');
  switchElement.addEventListener('click', () => {
    for (const item of radiosInput) {
      if (!item.checked) {
        item.click();
        break;
      }
    }
  });
}
