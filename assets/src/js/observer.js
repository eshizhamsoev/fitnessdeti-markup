import { loadScript } from './loadScript';

const options = {
  root: null,
  rootMargin: '300px',
  threshold: 0,
};
const observer = new IntersectionObserver((entries) => {
  entries.forEach((entry) => {
    if (entry.isIntersecting) {
      const loader = document.querySelector('.js-loader');
      loader.classList.add('active');

      const scriptApiYandex = loadScript(
        'https://api-maps.yandex.ru/2.1?lang=ru_RU'
      );
      const mapYandex = import('./mapSearch');
      observer.disconnect();
      Promise.all([scriptApiYandex, mapYandex])
        .then((values) => values[1].showMap())
        .then(() => {
          loader.classList.remove('active');
        })
        .catch((err) => console.error(err));
    }
  });
}, options);
const mapTarget = document.querySelector('.js-find-us');
if (mapTarget) {
  observer.observe(mapTarget);
}
