const nodeCalendar = document.querySelector('.calendar__inner');
const eventsCalendar = [
  {
    id: '101010101',
    title: '16:00',
    start: '2021-08-10',
  },
  {
    id: '21222222',
    title: '17:00',
    start: '2021-08-11',
  },
];
function nextMonth(...args) {
  for (const arg of args) {
    arg.setMonth(arg.getMonth() + 1);
  }
}

function prevMonth(...args) {
  for (const arg of args) {
    arg.setMonth(arg.getMonth() - 1);
  }
}

if (nodeCalendar) {
  const currentDate = new Date();
  const prevDate = new Date();
  const nextDate = new Date();
  prevDate.setMonth(prevDate.getMonth() - 1);
  nextDate.setMonth(nextDate.getMonth() + 1);
  const monthsArray = [prevDate, currentDate, nextDate];
  import('./calendar')
    .then(({ initCalendar }) => {
      initCalendar(eventsCalendar, nodeCalendar, monthsArray);
      const buttonsNext = document.querySelectorAll('.calendar__next');
      const buttonsPrev = document.querySelectorAll('.calendar__prev');
      for (const button of buttonsNext) {
        button.addEventListener('click', () => {
          nextMonth(currentDate, prevDate, nextDate);
          initCalendar(eventsCalendar, nodeCalendar, monthsArray);
        });
      }
      for (const button of buttonsPrev) {
        button.addEventListener('click', () => {
          prevMonth(currentDate, prevDate, nextDate);
          initCalendar(eventsCalendar, nodeCalendar, monthsArray);
        });
      }
    })
    .catch((error) => console.error(error));
}
