import tippy from 'tippy.js';
import 'tippy.js/dist/tippy.css';

const sport = document.querySelector('.form-input__question_sport');
const day = document.querySelector('.form-input__question_day');

if (sport) {
  tippy(sport, {
    content: 'На время занятие спортом',
  });
}

if (day) {
  tippy(day, {
    content: '24 часа в сутки (включая занятие спортом)',
  });
}
