import '@fancyapps/ui/dist/fancybox.css';
import { Fancybox } from '@fancyapps/ui';

Fancybox.bind('[data-src="#header__cities"]', {});

const fancyRequests = document.querySelectorAll('.fancy-request');
for (const item of fancyRequests) {
  const checkbox = item.querySelector('input[type="checkbox"]');
  const btn = item.querySelector('.fancy-request__btn');
  btn.disabled = true;
  checkbox.addEventListener('click', () => {
    btn.disabled = !checkbox.checked;
  });
}

document.body.addEventListener('click', (element) => {
  const { target } = element;
  if (target.closest('.js-form-modal-trigger')) {
    const { formHeading, formLeading, formSource } = target.dataset;
    const modalElement = document.querySelector('.js-fancy-modal');
    modalElement.querySelector('.js-heading').innerHTML =
      formHeading || 'Позвоните нам';
    modalElement.querySelector('.js-leading').innerHTML =
      formLeading || 'Оставьте ваши данные';
    modalElement.querySelector('.js-source').value = formSource;
    Fancybox.show([
      {
        src: '#fancy-modal',
        type: 'inline',
      },
    ]);
  }
});
