import { Calendar } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import { Fancybox } from '@fancyapps/ui';

const screenWidth = document.documentElement.clientWidth;

function showEvent() {
  // eslint-disable-next-line no-new
  new Fancybox([
    {
      src: '#modal-event',
      type: 'inline',
    },
  ]);
}

export function initCalendar(events, node, months) {
  const calendars = node.querySelectorAll('.calendar__full');
  for (const [index, calendarFull] of calendars.entries()) {
    const calendar = new Calendar(calendarFull, {
      plugins: [dayGridPlugin],
      initialView: 'dayGridMonth',
      headerToolbar: {
        left: false,
        center: 'title',
        right: false,
      },
      locale: 'ru',
      firstDay: 1,
      initialDate: months[index],
      height: 'auto',
      eventClick: showEvent,
      events,
    });
    if (screenWidth <= 1024) {
      calendar.setOption('headerToolbar', { left: 'prev', right: 'next' });
    }
    calendar.render();
  }
  node.querySelectorAll('.fc-daygrid-event-harness').forEach((e) => {
    const elem = e.parentElement.parentElement.parentElement;
    elem.style.backgroundColor = 'var(--color-green)';
    elem.style.color = 'var(--color-white)';
  });
}
