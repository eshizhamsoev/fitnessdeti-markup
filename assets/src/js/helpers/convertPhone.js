export function convertPhone(str) {
  let newStr = '+7';
  let start = 0;
  let step = 3;
  for (let i = 4; i > 0; i--) {
    newStr += '-';
    newStr += str.slice(start, start + step);
    start += step;
    if (i === 3) {
      step = 2;
    }
  }
  return newStr;
}
