export const centerCoords = [55.604534, 37.520069];
export const iconSize = [53, 61];
export const offsetIcon = [-26.5, -61];
export const iconSizeHover = [78, 91];
export const offsetIconHover = [-39, -91];
