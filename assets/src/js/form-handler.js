import axios from 'axios';
import { Fancybox } from '@fancyapps/ui';

document.addEventListener('submit', (e) => {
  const form = e.target;
  if (!form.classList.contains('js-form')) {
    return;
  }
  e.preventDefault();
  form.dataset.processing = 'true';
  const alert = form.querySelector('.js-form-alert');

  axios
    .post(form.action, new FormData(form))
    .then(() => {
      new Fancybox([
        {
          src: '#fancy-modal-success',
          type: 'inline',
        },
      ]);
    })
    .catch((error) => {
      if (error.response.status !== 422) {
        throw new Error('Wrong response');
      }
      alert.textContent = Object.values(error.response.data.errors)
        .flat()
        .join(' ');
    })
    .catch(() => {
      new Fancybox([
        {
          src: '#fancy-modal-error',
          type: 'inline',
        },
      ]);
    })
    .finally(() => {
      form.dataset.processing = null;
    });
});
